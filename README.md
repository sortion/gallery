# [gallery.samuel.ortion.fr](https://gallery.ortion.fr)

The [thumbsup](https://thumbsup.github.io/) powered gallery from Samuel Ortion

## Setup

* Use git LFS, install `git-lfs`.

* Clone the repository:

    ```bash
    git clone git@framagit.org:sortion/gallery.git
    ```

* Install `thumbsup`:
  Using [nvm](https://nvm.sh) is recommended. `thumbsup` is a node module, so may install it using `npm`:
    ```bash
    npm install -g thumbsup
    ```

* Render the static gallery:
    ```bash
    npm run render
    ```

* See the results locally: 
  One might want to use `http-server` npm module to quickly see the generated website:
  ```bash
  http-server public
  ```
   

### Using Git LFS for images

```bash
git lfs install
```

```bash
git lfs track/*
```